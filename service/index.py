#this is a mock API. it lacks auth and has a poor REST structure for GET/POST, etc. it's a mock to illustrate mobile app functionality.

import os, os.path
import cherrypy
import json
from bson.objectid import ObjectId
from pymongo import Connection

#MongoDB stuff
server = 'localhost'
port = 27017
db_name = 'ionic0'
conn = Connection(server, port)
db = conn[db_name]
user = db.user

#Handles encoding of MongoDB ObjectId
class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        else:
            return obj
        
class Root:
    @cherrypy.expose
    def index(self):
        resp = []
        for item in user.find():
            resp.append(item)
        return json.dumps(resp, cls=Encoder)
    
    @cherrypy.expose
    def update(self, id):
        update = {'id':id,'success':True}
        return json.dumps(update)
    
    @cherrypy.expose
    def sync(self, items):
        sync = []
        #for item in json.dumps(items):
        #    print(item[0])
        return json.dumps(items, cls=Encoder)
 
cherrypy.config.update({'server.socket_host': '0.0.0.0',})
cherrypy.config.update({'server.socket_port': int(os.environ.get('PORT', '5050')),})
cherrypy.quickstart(Root())